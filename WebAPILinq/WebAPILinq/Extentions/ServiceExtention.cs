using Microsoft.Extensions.DependencyInjection;
using WebAPILinq.BLL.Services;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.UnitOfWork;


namespace WebAPILinq.Extentions
{
    public static class ServiceExtention
    {
        public static void AddProjectServices(this IServiceCollection services)
        {
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IPositionService, PositionService>();
            services.AddScoped<ITaskStateService, TaskStateService>();
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}