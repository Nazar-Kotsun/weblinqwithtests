using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class TaskStateController : Controller
    {
        private readonly ITaskStateService _taskStateService;

        public TaskStateController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        [HttpGet]
        [Route("taskStates")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _taskStateService.GetAllTaskStates();
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("taskStates")]
        public async Task<IActionResult> AddTaskStates([FromBody] TaskStateModel taskStateModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            
            try
            {
                await _taskStateService.AddTaskState(taskStateModel);
                return Created("", "State was created!");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpDelete]
        [Route("taskStates/{taskStateId}")]
        public async Task<IActionResult> DeleteTaskState(int taskStateId)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                await _taskStateService.DeleteTaskState(taskStateId);
                return Ok("State was deleted");
            }
            catch (InvalidOperationException ex)
            {
                strMistakes = ex.Message;
            }
            catch (NullReferenceException ex)
            {
                strMistakes = ex.Message;
            }
            catch (Exception ex)
            {
                strMistakes = ex.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpPut]
        [Route("taskStates")]
        public async Task<IActionResult> UpdateTaskStates(TaskStateModel taskStateModel)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                await _taskStateService.UpdateTaskState(taskStateModel);
                return Ok("State was updated");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
    }
}