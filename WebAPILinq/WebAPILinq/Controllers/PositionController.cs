using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.Services.Interfaces;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Controllers
{
    [ApiController]
    [Route("api/")]
    public class PositionController : Controller
    {
        
        private readonly IPositionService _positionService;

        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }

        [HttpGet]
        [Route("positions")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _positionService.GetAllPositions();
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            if (result.Any())
            {
                return Ok(result);
            }
            else
            {
                return NotFound();
            }
        }
        
        [HttpPost]
        [Route("positions")]
        public async Task<IActionResult> AddPosition([FromBody] PositionModel positionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            string strMistakes;
            
            try
            {
                await _positionService.AddPosition(positionModel);
                return Created("", "Position was created!");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
        [HttpDelete]
        [Route("positions/{positionID}")]
        public async Task<IActionResult> DeletePosition(int positionID)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                await _positionService.DeletePosition(positionID);
                return Ok("Position was deleted");
            }
            catch (InvalidOperationException ex)
            {
                strMistakes = ex.Message;
            }
            catch (NullReferenceException ex)
            {
                strMistakes = ex.Message;
            }
            catch (Exception ex)
            {
                strMistakes = ex.Message;
            }

            return BadRequest(strMistakes);
        }

        [HttpPut]
        [Route("positions")]
        public async Task<IActionResult> UpdatePosition(PositionModel positionModel)
        {
            string strMistakes;
            
            if (!ModelState.IsValid)
            {
                return BadRequest("Body request is bad");
            }
            
            try
            {
                await _positionService.UpdatePosition(positionModel);
                return Ok("Position was updated");
            }
            catch (InvalidCastException e)
            {
                strMistakes = e.Message;
            }
            catch (NullReferenceException e)
            {
                strMistakes = e.Message;
            }
            catch (Exception e)
            {
                strMistakes = e.Message;
            }

            return BadRequest(strMistakes);
        }
        
    }
}