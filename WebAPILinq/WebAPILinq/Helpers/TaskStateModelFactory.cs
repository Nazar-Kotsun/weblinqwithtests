using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Helpers
{
    public static class TaskStateModelFactory
    {
        public static IEnumerable<TaskStateModel> CreateTaskStates()
        {
            return new List<TaskStateModel>
            {
                new TaskStateModel
                {
                    Id = 1,
                    Value = "Created"
                },
                new TaskStateModel
                {
                    Id = 2,
                    Value = "Started"
                },
                new TaskStateModel
                {
                    Id = 3,
                    Value = "Finished"
                },
                new TaskStateModel
                {
                    Id = 4,
                    Value = "Canceled"
                }
            };
        }
    } 
}