namespace WebAPILinq.DAL.Models
{
    public class TaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}