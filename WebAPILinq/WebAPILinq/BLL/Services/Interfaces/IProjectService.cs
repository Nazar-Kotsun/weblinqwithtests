using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAPILinq.BLL.DTO;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectModel>>  GetAllProjects();
        Task<IEnumerable<ProjectTasksDTO>> GetProjectsWithCountOfTasksByUserId(int userId);
        Task<IEnumerable<ProjectDetailedDTO>> GetProjectsDetailed();
        Task AddProject(ProjectModel project);

        Task DeleteProject(int projectId);

        Task UpdateProject(ProjectModel projectModel);
    }
}