using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.BLL.Services.Interfaces
{
    public interface ITaskStateService
    {
        Task<IEnumerable<TaskStateModel>> GetAllTaskStates();
        Task AddTaskState(TaskStateModel taskStateModel);
        Task DeleteTaskState(int taskStateId);
        Task UpdateTaskState(TaskStateModel taskStateModel);
    }
}