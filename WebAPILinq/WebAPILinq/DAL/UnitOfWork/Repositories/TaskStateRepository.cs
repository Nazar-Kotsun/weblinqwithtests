using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase;

namespace WebAPILinq.DAL.UnitOfWork.Repositories
{
    public class TaskStateRepository : IBaseRepository<TaskStateModel>
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly DbSet<TaskStateModel> _dbSet;

        public TaskStateRepository(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
            _dbSet = projectsLinqContext.Set<TaskStateModel>();
        }

        public ValueTask<EntityEntry<TaskStateModel>> Create(TaskStateModel entity) => _dbSet.AddAsync(entity);

        public Task<TaskStateModel> GetById(int id) => _dbSet.AsNoTracking()
            .FirstOrDefaultAsync(t => t.Id == id);

        public Task<List<TaskStateModel>> GetAll() => _dbSet.AsNoTracking().ToListAsync();

        public async Task Update(TaskStateModel entity)
        {
            var local = await GetById(entity.Id);
            if (local != null)
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task Delete(int id) => _dbSet.Remove(await GetById(id));
    }
}