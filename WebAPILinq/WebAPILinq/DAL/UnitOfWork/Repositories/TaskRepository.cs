using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase;
using WebAPILinq.Helpers;

namespace WebAPILinq.DAL.UnitOfWork.Repositories
{
    public class TaskRepository : IBaseRepository<TaskModel>
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly DbSet<TaskModel> _dbSet;
        public TaskRepository(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
            _dbSet = _dbContext.Set<TaskModel>();
        }

        public ValueTask<EntityEntry<TaskModel>> Create(TaskModel entity) => _dbSet.AddAsync(entity);
        
        public Task<TaskModel> GetById(int id) => _dbSet.AsNoTracking().FirstOrDefaultAsync((task) => task.Id == id);

        public Task<List<TaskModel>> GetAll() => _dbSet.AsNoTracking().ToListAsync();

        public async Task Update(TaskModel entity)
        {
            var local = await GetById(entity.Id);
            if (local != null)
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task Delete(int id) => _dbSet.Remove(await GetById(id));

    }
}