using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WebAPILinq.DAL.Models;
using WebAPILinq.DataBase;
using WebAPILinq.Helpers;

namespace WebAPILinq.DAL.UnitOfWork.Repositories
{
    public class TeamRepository: IBaseRepository<TeamModel>
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly DbSet<TeamModel> _dbSet;

        public TeamRepository(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
            _dbSet = _dbContext.Set<TeamModel>();
        }

        public ValueTask<EntityEntry<TeamModel>> Create(TeamModel entity) => _dbSet.AddAsync(entity);

        public Task<TeamModel> GetById(int id) => _dbSet.AsNoTracking().FirstOrDefaultAsync((team) => team.Id == id);
        public Task<List<TeamModel>> GetAll() => _dbSet.ToListAsync();

        public async Task Update(TeamModel entity)
        {
            var local = await GetById(entity.Id);
            if (local != null)
            {
                _dbContext.Entry(local).State = EntityState.Detached;
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task Delete(int id) => _dbSet.Remove(await GetById(id));
    }
}