using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;
using WebAPILinq.DAL.UnitOfWork.Repositories;
using WebAPILinq.DataBase;


namespace WebAPILinq.DAL.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        private TaskRepository _taskRepository;
        private ProjectRepository _projectRepository;
        private UserRepository _userRepository;
        private TeamRepository _teamRepository;
        private PositionRepository _positionRepository;
        private TaskStateRepository _taskStateRepository;
        
        private readonly ProjectsLinqContext _dbContext;

        public UnitOfWork(ProjectsLinqContext projectsLinqContext)
        {
            _dbContext = projectsLinqContext;
        }
        
        public IBaseRepository<TaskModel> TaskRepository => _taskRepository ??= new TaskRepository(_dbContext);
        public IBaseRepository<ProjectModel> ProjectRepository => _projectRepository ??= new ProjectRepository(_dbContext);
        public IBaseRepository<UserModel> UserRepository => _userRepository ??= new UserRepository(_dbContext);
        public IBaseRepository<TeamModel> TeamRepository => _teamRepository ??= new TeamRepository(_dbContext);
        public IBaseRepository<PositionModel> PositionRepository =>
            _positionRepository ??= new PositionRepository(_dbContext);

        public IBaseRepository<TaskStateModel> TaskStateRepository =>
            _taskStateRepository ??= new TaskStateRepository(_dbContext);
        
        public Task<int> SaveChanges()
        {
            return _dbContext.SaveChangesAsync();
        }
    }
}