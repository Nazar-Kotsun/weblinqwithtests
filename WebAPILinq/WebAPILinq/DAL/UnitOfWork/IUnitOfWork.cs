using System.Threading.Tasks;
using WebAPILinq.DAL.Models;
using WebAPILinq.DAL.UnitOfWork.Repositories;


namespace WebAPILinq.DAL.UnitOfWork
{
    public interface IUnitOfWork
    {
        IBaseRepository<TaskModel> TaskRepository { get; }
        IBaseRepository<ProjectModel> ProjectRepository { get; }
        IBaseRepository<UserModel> UserRepository { get; }
        IBaseRepository<TeamModel> TeamRepository { get; }
        IBaseRepository<PositionModel> PositionRepository { get; }
        IBaseRepository<TaskStateModel> TaskStateRepository { get; }
        public Task<int> SaveChanges();
    }
}