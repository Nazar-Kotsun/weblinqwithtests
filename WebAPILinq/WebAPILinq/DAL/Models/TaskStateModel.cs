using System.Collections.Generic;


namespace WebAPILinq.DAL.Models
{
    public class TaskStateModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}