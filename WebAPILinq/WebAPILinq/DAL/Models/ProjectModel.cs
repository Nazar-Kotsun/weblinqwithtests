using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json.Serialization;


namespace WebAPILinq.DAL.Models
{
    public class ProjectModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("description")]
        public string Description { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonPropertyName("deadline")]
        public DateTime Deadline { get; set; }
        [JsonPropertyName("authorId")]
        [ForeignKey("FK_User")]
        public int AuthorId { get; set; }
        [JsonPropertyName("teamId")]
        [ForeignKey("FK_Team")]
        public int TeamId { get; set; }
        [JsonPropertyName("price")]
        public decimal Price { get; set; }
        [JsonIgnore]
        public List<TaskModel> Tasks { get; set; }
        [JsonIgnore]
        
        public TeamModel Team { get; set; }
        [JsonIgnore]
        
        public UserModel Author { get; set; }
        
        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"Deadline: {Deadline}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}";
        }
    }
}