using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.DataBase.Configurations
{
    
    public class ProjectConfiguration : IEntityTypeConfiguration<ProjectModel>
    {
        public void Configure(EntityTypeBuilder<ProjectModel> builder)
        {
            builder.ToTable("Projects").HasKey(p => p.Id);
            builder.ToTable("Projects").Property(p => p.Name).HasMaxLength(1000);
            builder.ToTable("Projects").Property(p => p.Description).HasMaxLength(4000);
            builder.ToTable("Projects").Property(p => p.CreatedAt).IsRequired();
            builder.ToTable("Projects").Property(p => p.Deadline).IsRequired();
            builder.ToTable("Projects").Property(p => p.AuthorId).IsRequired();
            builder.ToTable("Projects").Property(p => p.TeamId).IsRequired();
            builder.ToTable("Projects").Property(p => p.Price).HasColumnType("money").IsRequired();
            
            builder.HasData(ProjectModelFactory.CreateProjects());
        }
    }
}