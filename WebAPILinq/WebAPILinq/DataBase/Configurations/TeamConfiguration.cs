using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.DataBase.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<TeamModel>
    {
        public void Configure(EntityTypeBuilder<TeamModel> builder)
        {
            builder.Property(t => t.Name).HasMaxLength(1000);
            builder.Property(t => t.CreatedAt).IsRequired();

            builder.HasData(TeamModelFactory.CreateTeams());

        }
    }
}