using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;

namespace WebAPILinq.DataBase.Configurations
{
    public class TaskStateConfiguration : IEntityTypeConfiguration<TaskStateModel>
    {
        public void Configure(EntityTypeBuilder<TaskStateModel> builder)
        {
            builder.Property(t => t.Value).IsRequired().HasMaxLength(100);

            builder.HasData(TaskStateModelFactory.CreateTaskStates());
        }
    }
    
}