using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using WebAPILinq.BLL.DTO;
using WebAPILinq.BLL.Services;
using WebAPILinq.DAL.Models;
using WebAPILinq.DAL.UnitOfWork;
using WebAPILinq.DataBase;
using WebAPILinq.Tests.Helpers;
using Xunit;

namespace WebAPILinq.Tests
{
    public class WebApiLinqTest
    {
        private readonly ProjectsLinqContext _dbContext;
        private readonly UnitOfWork _unitOfWork;
        private readonly ProjectService _projectService;
        private readonly UserService _userService;
        private readonly TaskService _taskService;
        private readonly TeamService _teamService;
        
        private DbContextOptions<ProjectsLinqContext> options = new DbContextOptionsBuilder<ProjectsLinqContext>()
            .UseInMemoryDatabase(databaseName: "DbProjectsLinq").Options;
        
        public WebApiLinqTest()
        {
            _dbContext = new ProjectsLinqContext(options);
            _unitOfWork = new UnitOfWork(_dbContext);
            _projectService = new ProjectService(_unitOfWork);
            _userService = new UserService(_unitOfWork);
            _taskService = new TaskService(_unitOfWork);
            _teamService = new TeamService(_unitOfWork);
        }
        
        [Fact]
        public async Task GetProjectsWithCountOfTasksByUserId_ThanShowAllProjectsWithCountOfTasks()
        {
            int userId = 2;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();
            List<ProjectModel> projects = ProjectFactoryForTest.CreateProjects();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            _dbContext.Set<ProjectModel>().AddRange(projects);

            _dbContext.SaveChanges();

            var actualProjects = await _projectService.GetProjectsWithCountOfTasksByUserId(userId);
            List<ProjectTasksDTO> actualListProjects = actualProjects.ToList();
            
            Assert.NotEmpty(actualProjects);
            Assert.All(actualListProjects, p => Assert.True(userId == p.AuthorId));
            
            Assert.Equal(3,actualListProjects[0].CountOfTasks);

            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task GetProjectsDetailed_ThanGetProjectsDetailedInformation()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();
            List<ProjectModel> projects = ProjectFactoryForTest.CreateProjects();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            _dbContext.Set<ProjectModel>().AddRange(projects);
            _dbContext.Set<TeamModel>().AddRange(teams);
            
            _dbContext.SaveChanges();

            var actualProjects = await _projectService.GetProjectsDetailed();
            
            List<ProjectDetailedDTO> actualListProjects = actualProjects.ToList();
            
            Assert.NotEmpty(actualListProjects);
            Assert.True(projects.Select(p => p.Id).SequenceEqual(actualListProjects.Select(p => p.Project.Id)));
            
            Assert.Equal(projects[0].Id, actualListProjects[0].TheLongestTask.Id);
            Assert.Equal(projects[2].Id, actualListProjects[0].TheShortedTask.Id);
            
            Assert.Equal(0, actualListProjects[0].CountOfUsers);
            Assert.Equal(0, actualListProjects[1].CountOfUsers);
            Assert.Equal(2, actualListProjects[2].CountOfUsers);
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetTasksByUserId_ThanGetAllTasksByUserId()
        {
            int userId = 1;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();
            
            var actualTasks = await _taskService.GetTasksByUserId(userId);
            List<TaskModel> actualListTasks = actualTasks.ToList();
            
            Assert.NotEmpty(actualListTasks);
            
            Assert.All(actualListTasks, t => Assert.Equal(userId, t.PerformerId));
            Assert.All(actualListTasks, t => Assert.True(t.Name.Length < 45));
            
            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task GetTasksFinishedInCurrentYear_ThanGetTasksFinishedInCurrentYear()
        {
            int userId = 1;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();

            var expectedList = tasks.Where(t => t.PerformerId == userId).Select(t => t.Id);
            
            var actualTasks = await _taskService.GetTasksFinishedInCurrentYear(userId);
            List<TaskDTO> actualListTasks = actualTasks.ToList();
             
            Assert.NotEmpty(actualListTasks);
            Assert.Equal(2, actualListTasks.Count);
            
            Assert.Equal(DateTime.Now.Year, tasks
                .FirstOrDefault(t => t.Id == actualListTasks[0].Id).FinishedAt.Year);
            Assert.True(expectedList.SequenceEqual(actualTasks.Select(t => t.Id)));

            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task UpdateTask_ThanUpdateInformationAboutTask()
        {
            int taskId = 5;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();
            List<ProjectModel> projects = ProjectFactoryForTest.CreateProjects();
            List<TaskStateModel> states = TaskSateFactoryForTest.CreateTaskStates();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            _dbContext.Set<ProjectModel>().AddRange(projects);
            _dbContext.Set<TaskStateModel>().AddRange(states);
            
            _dbContext.SaveChanges();

            TaskModel updateTask = tasks.FirstOrDefault(t => t.Id == taskId);
            updateTask.TaskStateId = 2;
            
            var unitFake = A.Fake<IUnitOfWork>();
            var taskServiceFake = new TaskService(unitFake);

            await taskServiceFake.UpdateTask(updateTask);
            
            A.CallTo(() => unitFake.TaskRepository.Update(updateTask)).MustHaveHappened();
            
            await _taskService.UpdateTask(updateTask);

            _dbContext.SaveChanges();

            var actualTask = (await _taskService.GetAllTasks())
                .FirstOrDefault(t => t.Id == updateTask.Id);
            
            Assert.Equal(updateTask.Id, actualTask.Id);
            Assert.Equal(2 , actualTask.TaskStateId);

            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetTeamsWithUsers_ThanGetTeamsWithUsers()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            
            _dbContext.Set<UserModel>().AddRange(users); 
            _dbContext.Set<TeamModel>().AddRange(teams);

            _dbContext.SaveChanges();

            var expectedSortedList = users.
                Where(u => u.TeamId == teams[0].Id).
                Select(u => u.RegisteredAt).
                OrderByDescending(u => u);
                
            var actualTeams = await _teamService.GetTeamsWithUsers();
            List<TeamUsersDTO> actualListTeams = actualTeams.ToList();
            
            Assert.NotEmpty(actualListTeams);
            Assert.Equal(teams[0].Id, actualListTeams[0].TeamId);
            
            Assert.All(actualListTeams[0].ListUsers, u=> 
                Assert.Equal(teams[0].Id, u.TeamId));
            
            Assert.All(actualListTeams[0].ListUsers, u => 
                Assert.True((u.RegisteredAt.Year - u.Birthday.Year) > 10));
            
            Assert.True(expectedSortedList.
                SequenceEqual(actualListTeams[0].ListUsers.Select(s => s.RegisteredAt)));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetUsersWithSortedTasks_ThanGetUsersWithSortedTasks()
        {
            int userId = 3;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();

            var expectedOrderListUserName = users.Select(u => u.FirstName).OrderBy(u => u);
            var expectedOrderListTaskNameLength = tasks.
                Where(t => t.PerformerId == userId).
                Select(t => t.Name.Length).
                OrderByDescending(t => t);
            
            var actualUsers = await _userService.GetUsersWithSortedTasks();
            List <UserTasksDTO> actualListUsers = actualUsers.ToList();
            
           Assert.True(expectedOrderListUserName.SequenceEqual(actualListUsers.Select(u => u.UserFirstName)));
           
           Assert.All(actualListUsers[1].ListTasks, t => Assert.Equal(userId, t.PerformerId));
           Assert.True(expectedOrderListTaskNameLength.SequenceEqual(actualListUsers[1].ListTasks.Select(t => t.Name.Length)));
           
           _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetDetailedInformByUserId_ThanGetDetailedInformationAboutUser()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();
            List<ProjectModel> projects = ProjectFactoryForTest.CreateProjects();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            _dbContext.Set<ProjectModel>().AddRange(projects);
            
            _dbContext.SaveChanges();

            var expectedCountOfTasks = tasks.Count(t => projects[0].Id == t.ProjectId);
            var expectedCountOfCanceledOrStarted = tasks.Count(t => 
                t.PerformerId == users[0].Id && (t.TaskStateId == (int)TaskState.Canceled || t.TaskStateId == (int)TaskState.Started));
            var expectedTheLongestTask = tasks.Where(t => t.PerformerId == users[0].Id)
                .OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault();
                
            var actualUserInformation = await _userService.GetDetailedInformByUserId(1);
            
            Assert.NotNull(actualUserInformation);
            Assert.Equal(users[0].Id, actualUserInformation.User.Id);
            Assert.Equal(users[0].Id, actualUserInformation.LastProject.AuthorId);
            Assert.Equal(projects[0].Id, actualUserInformation.LastProject.Id);
            Assert.Equal(expectedCountOfTasks, actualUserInformation.CountOfTasks);
            Assert.Equal(expectedCountOfCanceledOrStarted, actualUserInformation.CountOfStartedOrCanceledTasks);
            Assert.Equal(expectedTheLongestTask.Id, actualUserInformation.TheLongestTask.Id);
            
            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task AddUser_ThanPlusOneUser()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            List<PositionModel> positions = PositionFactoryForTest.CreatePositions();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            _dbContext.Set<PositionModel>().AddRange(positions);
            
            _dbContext.SaveChanges();

            UserModel newUser = new UserModel
            {
                Id = 4,
                FirstName = "Verla",
                LastName = "Bechtelar",
                Email = "Verla.Bechtelar62@gmail.com",
                Birthday = Convert.ToDateTime("2008-07-14T20:47:23.7087566+00:00"),
                RegisteredAt = Convert.ToDateTime("2020-07-01T05:22:15.869556+00:00"),
                TeamId = 2,
                PositionId = 3
            };
            
            var unitFake = A.Fake<IUnitOfWork>();
            var userServiceFake = new UserService(unitFake);

            await userServiceFake.AddUser(newUser);
            
            A.CallTo(() => unitFake.UserRepository.Create(newUser)).MustHaveHappened();

            await _userService.AddUser(newUser);
            
            _dbContext.SaveChanges();
            
            List<UserModel> actualUsers = (await _userService.GetAllUsers()).ToList();
            
            Assert.Equal(users.Count + 1, actualUsers.Count);
            Assert.Contains(actualUsers, u => u.Id == newUser.Id);

            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task UpdateUser_ThanUpdateTeamIdForUser()
        {
            int userId = 1;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            
            _dbContext.SaveChanges();
            
            UserModel updateUser = users.FirstOrDefault(t => t.Id == userId);
            updateUser.TeamId = 2;
            
            var unitFake = A.Fake<IUnitOfWork>();
            var taskUserService = new UserService(unitFake);

            await taskUserService.UpdateUser(updateUser);
            
            A.CallTo(() => unitFake.UserRepository.Update(updateUser)).MustHaveHappened();
            
            await _userService.UpdateUser(updateUser);

            _dbContext.SaveChanges();
            
            var actualUser = (await _userService.GetAllUsers())
                .FirstOrDefault(t => t.Id == updateUser.Id);
            
            Assert.Equal(updateUser.Id, actualUser.Id);
            Assert.Equal(2 , updateUser.TeamId);

            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetNotCompleteTasksByUserId_ThanShowNotCompleteTasksByUserId()
        {
            int userId = 3;
            
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();

            List<TaskModel> actualTasks = (await _taskService.GetNotCompleteTasksByUserId(userId)).ToList();
            
            Assert.NotEmpty(actualTasks);
            Assert.All(actualTasks, t => Assert.Equal(userId, t.PerformerId));
            Assert.All(actualTasks, t => 
                Assert.True(t.TaskStateId == (int)TaskState.Created 
                            || t.TaskStateId == (int)TaskState.Started ));

            _dbContext.Database.EnsureDeleted();
        }

        //////////////////////////////////////////////
        // negative tasks
        //////////////////////////////////////////////
        
        [Fact]
        public async Task UpdateTask_When_TaskStateNotExist_ThanUpdateInformationAboutTask()
        {
            int taskId = 5;
            
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();
            List<ProjectModel> projects = ProjectFactoryForTest.CreateProjects();
            List<TaskStateModel> states = TaskSateFactoryForTest.CreateTaskStates();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            _dbContext.Set<ProjectModel>().AddRange(projects);
            _dbContext.Set<TaskStateModel>().AddRange(states);
            
            _dbContext.SaveChanges();

            TaskModel updateTask = tasks.FirstOrDefault(t => t.Id == taskId);
            updateTask.TaskStateId = 10;
            
            _dbContext.SaveChanges();
            
            await Assert.ThrowsAsync<InvalidOperationException>(async () => 
                await _taskService.UpdateTask(updateTask));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task UpdateUser_WhenTeamNotExist_ThanThrowInvalidOperationException()
        {
            int userId = 1;
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            
            _dbContext.SaveChanges();
            
            UserModel updateUser = users.FirstOrDefault(t => t.Id == userId);
            updateUser.TeamId = 4;

            await Assert.ThrowsAsync<InvalidOperationException>(async () =>  await _userService.UpdateUser(updateUser));
            
            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task UpdateUser_WhenUserIsNull_ThanThrowNullReferenceException()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            
            _dbContext.SaveChanges();

            UserModel updateUser = null;

            await Assert.ThrowsAsync<NullReferenceException>(async () => 
                await _userService.UpdateUser(updateUser));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task AddUser_WhenUserAlreadyExist_ThanThrowInvalidOperationException()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            List<PositionModel> positions = PositionFactoryForTest.CreatePositions();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            _dbContext.Set<PositionModel>().AddRange(positions);
            
            _dbContext.SaveChanges();

            UserModel newUser = new UserModel
            {
                Id = 1,
                FirstName = "Verla",
                LastName = "Bechtelar",
                Email = "Verla.Bechtelar62@gmail.com",
                Birthday = Convert.ToDateTime("2008-07-14T20:47:23.7087566+00:00"),
                RegisteredAt = Convert.ToDateTime("2020-07-01T05:22:15.869556+00:00"),
                TeamId = 2,
                PositionId = 3
            };

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await _userService.AddUser(newUser));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task AddUser_WhenTeamNotExist_ThanThrowInvalidOperationException()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            List<PositionModel> positions = PositionFactoryForTest.CreatePositions();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            _dbContext.Set<PositionModel>().AddRange(positions);
            
            _dbContext.SaveChanges();

            UserModel newUser = new UserModel
            {
                Id = 5,
                FirstName = "Verla",
                LastName = "Bechtelar",
                Email = "Verla.Bechtelar62@gmail.com",
                Birthday = Convert.ToDateTime("2008-07-14T20:47:23.7087566+00:00"),
                RegisteredAt = Convert.ToDateTime("2020-07-01T05:22:15.869556+00:00"),
                TeamId = 10,
                PositionId = 3
            };

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await _userService.AddUser(newUser));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task AddUser_WhenPositionNotExist_ThanThrowInvalidOperationException()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            List<PositionModel> positions = PositionFactoryForTest.CreatePositions();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            _dbContext.Set<PositionModel>().AddRange(positions);
            
            _dbContext.SaveChanges();

            UserModel newUser = new UserModel
            {
                Id = 5,
                FirstName = "Verla",
                LastName = "Bechtelar",
                Email = "Verla.Bechtelar62@gmail.com",
                Birthday = Convert.ToDateTime("2008-07-14T20:47:23.7087566+00:00"),
                RegisteredAt = Convert.ToDateTime("2020-07-01T05:22:15.869556+00:00"),
                TeamId = 2,
                PositionId = 8
            };

            await Assert.ThrowsAsync<InvalidOperationException>(async ()
                => await _userService.AddUser(newUser));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task AddUser_WhenUserIsNull_ThanThrowNullReferenceException()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TeamModel> teams = TeamFactoryForTest.CreateTeams();
            List<PositionModel> positions = PositionFactoryForTest.CreatePositions();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TeamModel>().AddRange(teams);
            _dbContext.Set<PositionModel>().AddRange(positions);
            
            _dbContext.SaveChanges();

            UserModel newUser = null;

            await Assert.ThrowsAsync<NullReferenceException>(async ()
                => await _userService.AddUser(newUser));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetDetailedInformByUserId_WhenUserNotExist_ThanGetDetailedInformationAboutUser()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();
            List<ProjectModel> projects = ProjectFactoryForTest.CreateProjects();
            
            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            _dbContext.Set<ProjectModel>().AddRange(projects);
            
            _dbContext.SaveChanges();

            await Assert.ThrowsAsync<InvalidOperationException>(async () => 
                await _userService.GetDetailedInformByUserId(10));
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetTasksFinishedInCurrentYear_When_UserNotExist_ThanGetTasksFinishedInCurrentYear()
        {
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();

            List<TaskDTO> listTasks = (await _taskService.GetTasksFinishedInCurrentYear(20)).ToList();
            
            Assert.Empty(listTasks);
            
            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetTasksByUserId_When_UserNotExist_ThanGetAllTasksByUserId()
        {
           
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();
            
            List<TaskModel> actualTasks = (await _taskService.GetTasksByUserId(20)).ToList();
            
            Assert.Empty(actualTasks);
            
            _dbContext.Database.EnsureDeleted();
        }

        [Fact]
        public async Task GetNotCompleteTasksByUserId_WhenUserNotExist_ThanInvalidOperationException()
        {
            int userId = 7;
            
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();

            await Assert.ThrowsAsync<InvalidOperationException>( async () 
                => await _taskService.GetNotCompleteTasksByUserId(userId));

            _dbContext.Database.EnsureDeleted();
        }
        
        [Fact]
        public async Task GetNotCompleteTasksByUserId_WhenUserDontHaveNotCompleteTasks_ThanGetEmptyList()
        {
            int userId = 2;
            
            List<UserModel> users = UserFactoryForTest.CreateUsers();
            List<TaskModel> tasks = TaskFactoryForTest.CreateTasks();

            _dbContext.Set<UserModel>().AddRange(users);
            _dbContext.Set<TaskModel>().AddRange(tasks);
            
            _dbContext.SaveChanges();

            List<TaskModel> actualTasks = (await _taskService.GetNotCompleteTasksByUserId(userId)).ToList();

            Assert.Empty(actualTasks);
            
            _dbContext.Database.EnsureDeleted();
        }
    }
}