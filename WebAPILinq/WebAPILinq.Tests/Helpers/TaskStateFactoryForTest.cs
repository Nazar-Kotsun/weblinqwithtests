using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Tests.Helpers
{
    public static class TaskSateFactoryForTest
    {
        public static List<TaskStateModel> CreateTaskStates()
        {
            return new List<TaskStateModel>
            {
                new TaskStateModel
                {
                    Id = 1,
                    Value = "Created"
                },
                new TaskStateModel
                {
                    Id = 2,
                    Value = "Started"
                },
                new TaskStateModel
                {
                    Id = 3,
                    Value = "Finished"
                },
                new TaskStateModel
                {
                    Id = 4,
                    Value = "Canceled"
                }
            };
        }
    }
}