using System;
using System.Collections.Generic;
using WebAPILinq.DAL.Models;

namespace WebAPILinq.Tests.Helpers
{
    public static class TeamFactoryForTest
    {
        public static List<TeamModel> CreateTeams()
        {
            return new List<TeamModel>
            {
                new TeamModel
                {
                    Id = 1,
                    Name = "debitis",
                    CreatedAt = Convert.ToDateTime("2020-06-30T20:20:46.3968861+00:00")
                },
                new TeamModel
                {
                    Id = 2,
                    Name = "cupiditate",
                    CreatedAt = Convert.ToDateTime("2020-06-30T18:46:57.0010953+00:00")
                }
            };
        }
    }
}