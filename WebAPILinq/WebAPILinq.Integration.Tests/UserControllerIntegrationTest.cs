using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using WebAPILinq.DAL.Models;
using WebAPILinq.Helpers;
using Xunit;

namespace WebAPILinq.Integration.Tests
{
    public class UserControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;
        
        public UserControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task DeleteUser_ThanResponseWithCodeOk()
        {
            _client = _factory.CreateClient();
            
            int userId = 2;
            var responseMessage = await _client.GetAsync("api/users");
            List<UserModel> beforeUsers = JsonSerializer.Deserialize<IEnumerable<UserModel>>
                (await responseMessage.Content.ReadAsStringAsync()).ToList();
            
            var userModel = beforeUsers.FirstOrDefault(u => u.Id == userId);
            var responseMessageDelete = await _client.DeleteAsync("api/users/" + userId);
            
            responseMessage = await _client.GetAsync("api/users");
            List<UserModel> afterUsers = JsonSerializer.Deserialize<List<UserModel>>
                (await responseMessage.Content.ReadAsStringAsync());
            
            
            Assert.Equal(HttpStatusCode.OK, responseMessageDelete.StatusCode);
            Assert.True(afterUsers.Count < beforeUsers.Count);
        }
        
        [Fact]
        public async Task DeleteUser_WhenUserNorExist_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            
            int userId = 10;
            var responseMessage = await _client.GetAsync("api/users");
            List<UserModel> beforeUsers = JsonSerializer.Deserialize<IEnumerable<UserModel>>
                (await responseMessage.Content.ReadAsStringAsync()).ToList();
            
            var userModel = beforeUsers.FirstOrDefault(u => u.Id == userId);
            var responseMessageDelete = await _client.DeleteAsync("api/users/" + userId);
            
            responseMessage = await _client.GetAsync("api/users");
            List<UserModel> afterUsers = JsonSerializer.Deserialize<List<UserModel>>
                (await responseMessage.Content.ReadAsStringAsync());

            Assert.Equal(HttpStatusCode.BadRequest, responseMessageDelete.StatusCode);
            Assert.Equal(afterUsers.Count, beforeUsers.Count);
        }

        [Fact]
        public async Task AddUser_ThanResponseWithCodeCreated()
        {
            _client = _factory.CreateClient();
            
            UserModel newUser = new UserModel
            {
                Id = 5,
                FirstName = "Retha",
                LastName = "Will",
                Email = "Retha67@yahoo.com",
                Birthday = Convert.ToDateTime("2007-09-20T10:05:41.1527935+00:00"),
                RegisteredAt = Convert.ToDateTime("2020-06-23T18:09:18.5038054+00:00"),
                TeamId = 2,
                PositionId = 1
            };
            
            string strSerialize = JsonSerializer.Serialize<UserModel>(newUser);

            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            var responseMessage = await _client.PostAsync("api/users", httpContent);

            UserModel createdUser =
                JsonSerializer.Deserialize<UserModel>(await responseMessage.Content.ReadAsStringAsync());
            
            Assert.Equal(HttpStatusCode.Created, responseMessage.StatusCode);
            Assert.Equal(newUser.Id, createdUser.Id);
        }
        
        [Fact]
        public async Task AddUser_WhenUserAlreadyExist_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            
            UserModel newUser = new UserModel
            {
                Id = 1,
                FirstName = "Retha",
                LastName = "Will",
                Email = "Retha67@yahoo.com",
                Birthday = Convert.ToDateTime("2007-09-20T10:05:41.1527935+00:00"),
                RegisteredAt = Convert.ToDateTime("2020-06-23T18:09:18.5038054+00:00"),
                TeamId = 2,
                PositionId = 1
            };
            
            string strSerialize = JsonSerializer.Serialize<UserModel>(newUser);

            HttpContent httpContent = new StringContent(strSerialize, Encoding.UTF8, "application/json");
            var responseMessage = await _client.PostAsync("api/users", httpContent);
            
            Assert.Equal(HttpStatusCode.BadRequest, responseMessage.StatusCode);
        }
    }
}