using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using WebAPILinq.DAL.Models;
using Xunit;

namespace WebAPILinq.Integration.Tests
{
    public class TeamControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public TeamControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task AddTeam_ThanResponseWithCodeCreated()
        {
            _client = _factory.CreateClient();
            
            TeamModel newTeam = new TeamModel
            {
                Id = 3,
                Name = "eaaa",
                CreatedAt = Convert.ToDateTime("2020-06-30T18:46:57.0010953+00:00")
            };
            
            string strSerializeVehicle = JsonSerializer.Serialize<TeamModel>(newTeam);

            HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
            var responseMessage = await _client.PostAsync("api/teams", httpContent);

            TeamModel createdTeam = JsonSerializer.Deserialize<TeamModel>( await responseMessage.Content.ReadAsStringAsync());
            
            Assert.Equal(HttpStatusCode.Created, responseMessage.StatusCode);
            Assert.Equal(newTeam.Id, createdTeam.Id);
        }
        
        [Fact]
        public async Task AddTeam_WhenTeamAlreadyExist_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            
            TeamModel newTeam = new TeamModel
            {
                Id = 2,
                Name = "eaaa",
                CreatedAt = Convert.ToDateTime("2020-06-30T18:46:57.0010953+00:00")
            };
            
            string strSerializeVehicle = JsonSerializer.Serialize<TeamModel>(newTeam);

            HttpContent httpContent = new StringContent(strSerializeVehicle, Encoding.UTF8, "application/json");
            var responseMessage = await _client.PostAsync("api/teams", httpContent);

            Assert.Equal(HttpStatusCode.BadRequest, responseMessage.StatusCode);
        }
    }
}