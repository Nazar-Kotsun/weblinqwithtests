using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using WebAPILinq.DAL.Models;
using Xunit;

namespace WebAPILinq.Integration.Tests
{
    public class TaskControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private HttpClient _client;
        private CustomWebApplicationFactory<Startup> _factory;
        
        public TaskControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task DeleteTask_ThanResponseWithCodeOk()
        {
            _client = _factory.CreateClient();
            int taskId = 3;

            var httpResponse = await _client.GetAsync("api/tasks");
            List<TaskModel> beforeTasks = JsonSerializer.Deserialize<IEnumerable<TaskModel>>
                (await httpResponse.Content.ReadAsStringAsync()).ToList();
            
            var httpResponseDelete = await _client.DeleteAsync("api/tasks/" + taskId);
            
            httpResponse = await _client.GetAsync("api/tasks");
            List<TaskModel> afterTasks = JsonSerializer.Deserialize<IEnumerable<TaskModel>>
                (await httpResponse.Content.ReadAsStringAsync()).ToList();
            
            Assert.Equal(HttpStatusCode.OK, httpResponseDelete.StatusCode);
            Assert.True(afterTasks.Count < beforeTasks.Count);
        }
        
        [Fact]
        public async Task DeleteTaskWhenTaskNotExist_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            int taskId = 10;

            var httpResponse = await _client.GetAsync("api/tasks");
            List<TaskModel> beforeTasks = JsonSerializer.Deserialize<IEnumerable<TaskModel>>
                (await httpResponse.Content.ReadAsStringAsync()).ToList();
            
            var httpResponseDelete = await _client.DeleteAsync("api/tasks/" + taskId);
            
            httpResponse = await _client.GetAsync("api/tasks");
            List<TaskModel> afterTasks = JsonSerializer.Deserialize<IEnumerable<TaskModel>>
                (await httpResponse.Content.ReadAsStringAsync()).ToList();
            
            Assert.Equal(HttpStatusCode.BadRequest, httpResponseDelete.StatusCode);
            Assert.Equal(afterTasks.Count,beforeTasks.Count);
        }

        [Fact]
        public async Task GetNotCompleteTasksByUserId_ThanResponseWithCodeOK()
        {
            _client = _factory.CreateClient();
            int userId = 3;

            var httpResponse = await _client.GetAsync("api/tasks/notComplete/" + userId);
            List<TaskModel> actualTasks = JsonSerializer.Deserialize<IEnumerable<TaskModel>>
                (await httpResponse.Content.ReadAsStringAsync()).ToList();
            
            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.All(actualTasks, t => Assert.Equal(userId, t.PerformerId));
            Assert.All(actualTasks, t => 
                Assert.True(t.TaskStateId == (int)TaskState.Created 
                            || t.TaskStateId == (int)TaskState.Started ));
        }
        
        [Fact]
        public async Task GetNotCompleteTasksByUserId_WhenUserNotExist_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            int userId = 10;

            var httpResponse = await _client.GetAsync("api/tasks/notComplete/" + userId);
            
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
        
        [Fact]
        public async Task GetNotCompleteTasksByUserId_WhenUserDontHaveNotCompleteProjects_ThanResponseWithCodeBadRequest()
        {
            _client = _factory.CreateClient();
            int userId = 2;

            var httpResponse = await _client.GetAsync("api/tasks/notComplete/" + userId);
            
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
        
    }
}