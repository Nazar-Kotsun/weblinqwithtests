using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class TaskModel
    {
        [JsonPropertyName("id")] 
        public int Id { get; set; }
        
        [JsonPropertyName("name")] 
        public string Name { get; set; }
        
        [JsonPropertyName("description")] 
        public string Description { get; set; }
        
        [JsonPropertyName("state")] 
        public TaskState State { get; set; }
        
        [JsonPropertyName("createdAt")] 
        public DateTime CreatedAt { get; set; }
        
        [JsonPropertyName("finishedAt")] 
        public DateTime FinishedAt { get; set; }
        
        [JsonPropertyName("projectId")] 
        public int ProjectId { get; set; }
        
        [JsonPropertyName("performerId")] 
        public int PerformerId { get; set; }
        
        [JsonPropertyName("taskStateId")]
        public int TaskStateId { get; set; }
        //
        // public User Performer { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Name: {Name}\n" +
                   $"Description: {Description}\n" +
                   $"State: {State}\n" +
                   $"CreatedAt: {CreatedAt}\n" +
                   $"FinishedAt: {FinishedAt}\n" +
                   $"ProjectId: {ProjectId}\n" +
                   $"PerformerId: {PerformerId}";
        }
        
    }
}