using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class TaskStateModel
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [JsonPropertyName("value")]
        public string Value { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Value: {Value}";
        }
    }
}