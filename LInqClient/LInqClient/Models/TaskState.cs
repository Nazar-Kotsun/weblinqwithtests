namespace LInqClient.Models
{
    public enum TaskState
    {
        Created, Started, Finished, Canceled
    }
}