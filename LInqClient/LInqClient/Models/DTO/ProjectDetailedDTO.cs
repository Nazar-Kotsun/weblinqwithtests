using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models.DTO
{
    public class ProjectDetailedDTO
    {
        [JsonPropertyName("project")]
        public Project Project { get; set; }
        
        [JsonPropertyName("theLongestTask")]
        public TaskModel TheLongestTask{ get; set; }
        
        [JsonPropertyName("theShortedTask")]
        public TaskModel TheShortedTask { get; set; }
        
        [JsonPropertyName("countOfUsers")]
        public int? CountOfUsers { get; set; }
    }
}