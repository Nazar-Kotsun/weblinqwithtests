using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models.DTO
{
    public class UserDetailedDTO
    {
        [JsonPropertyName("user")]
        public User User { get; set; }
        
        [JsonPropertyName("lastProject")]
        public Project LastProject { get; set; }
        
        [JsonPropertyName("countOfTasks")]
        public int CountOfTasks { get; set; }
        
        [JsonPropertyName("countOfStartedOrCanceledTasks")]
        public int CountOfStartedOrCanceledTasks { get; set; }
        
        [JsonPropertyName("theLongestTask")]
        public TaskModel TheLongestTask { get; set; }
        
    }
}