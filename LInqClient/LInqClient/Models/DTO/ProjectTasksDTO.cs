using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class ProjectTasksDTO
    {
        [JsonPropertyName("projectId")] 
        public int ProjectId { get; set; }

        [JsonPropertyName("projectName")] 
        public string ProjectName { get; set; }

        [JsonPropertyName("authorId")] 
        public int AuthorId { get; set; }
        
        [JsonPropertyName("teamId")] 
        public int TeamId { get; set; }
        
        [JsonPropertyName("countOfTasks")]
        public int CountOfTasks { get; set; }

        public override string ToString()
        {
            return $"Id: {ProjectId}\n" +
                   $"Name: {ProjectName}\n" +
                   $"AuthorId: {AuthorId}\n" +
                   $"TeamId: {TeamId}\n" +
                   $"CountOfTasks: {CountOfTasks}" ;
        }
    }
}