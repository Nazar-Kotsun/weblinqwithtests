using System.Text.Json.Serialization;

namespace LInqClient.Models
{
    public class Position
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        [JsonPropertyName("name")]
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\n" +
                   $"Value: {Name}";
        }
    }
}